import React, { Component } from 'react';
import { Table, Row, Col } from './components/FlexboxTable';

// Stylesheet
import './stylesheets/styles.css';


export default class App extends Component {

  render() {
    const sampleData = [
      {
        title: "Document Title",
        lastUpdatedIn: "Live",
        lastModifiedBy: "Intranet Administrator",
        lastModified: "5/18/2017 12:54 PM",
        fileType: "docx",
        fileSize: "13 KB"
      },
      {
        title: "Another Very Loooooong Presentation Document Title",
        lastUpdatedIn: "Live",
        lastModifiedBy: "Alice Richardson",
        lastModified: "",
        fileType: "pptx",
        fileSize: "2 MB"
      }
    ];

    return (
      <div className="app">

        <h1 className="app__header">React Component: iD CMS Responsive Flexbox Table SASS</h1>

        <div className="container">
          
          {/* Code box */}
          <div className="code-box">
            <h3>Usage</h3>
            <pre>
              {` 
  import React from 'react';
  import { Table, Row, Col } from './components/FlexboxTable';
  import './stylesheets/styles.css';

  ReactDOM.Render(
    <Table>

      //First row SHOULD have rowHeader prop
      <Row rowHeader>
        <Col>...</Col>
        <Col>...</Col>
      </Row>

      <Row>
        <Col>...</Col>
        <Col>...</Col>
      </Row>

    </Table>
  );
              `}
            </pre>
          </div>

          <div className="demo-box">
            <h3>Demo: Responsive Default View</h3>

            {/* Default Mobile View */}
            <div className="actions-pane">Actions Pane...</div>
            <Table>

              {/* Define Table Headers */}
              <Row key={0} rowHeader>
                <Col className="ft-col_checkbox" columnTitle="checkbox">
                  <div className="ft-checkbox-holder">
                    <input type="checkbox" id="ft_checkbox_0" className="ft-checkbox" />  
                    <label className="ft-checkbox__label" htmlFor="ft_checkbox_0">&nbsp;</label> 
                  </div>
                </Col>
                <Col className="ft-col_title" columnTitle={"title"}>Title</Col>
                <Col className="ft-show_sm" columnTitle={"last-updated-in"}>Last Updated In</Col>
                <Col columnTitle={"last-modified-by"}>Last Modified by</Col>
                <Col columnTitle={"last-modified"}>Last Modified</Col>
                <Col className="ft-col_no-flex-grow ft-show_xs" columnTitle={"type"}>Type</Col>
                <Col className="ft-col_no-flex-grow" columnTitle={"size"}>Size</Col>
              </Row>

              {sampleData.map((data, i) => {
                return (
                  <Row key={(i+1)}>
                    <Col className="ft-col_checkbox" columnTitle="checkbox">
                      <div className="ft-checkbox-holder">
                        <input type="checkbox" id={"ft_checkbox_" + (i+1)} className="ft-checkbox" />  
                        <label className="ft-checkbox__label" htmlFor={"ft_checkbox_" + (i+1)}>&nbsp;</label> 
                      </div>
                    </Col>
                    <Col className="ft-col_title" columnTitle={"title"} fileType={data.fileType}>
                      <a>{data.title}</a>
                    </Col>
                    <Col className="ft-show_sm" columnTitle={"last-updated-in"}>{data.lastUpdatedIn}</Col>
                    <Col columnTitle={"last-modified-by"}>{data.lastModifiedBy}</Col>
                    <Col columnTitle={"last-modified"}>{data.lastModified}</Col>
                    <Col className="ft-col_no-flex-grow ft-show_xs" columnTitle={"type"}>{data.fileType}</Col>
                    <Col className="ft-col_no-flex-grow" columnTitle={"size"}>{data.fileSize}</Col>
                  </Row>
                );
              })}

            </Table>
          </div>

          <div className="demo-box">
            <h3>Demo: Responsive Mobile Compact List View</h3>

            {/* Compact List Mobile View */}
            <div className="actions-pane">Actions Pane...</div>
            <Table mobileCompactList>

              {/* Define Table Headers*/}
              <Row key={0} rowHeader>
                <Col className="ft-col_checkbox" columnTitle="checkbox">
                  <div className="ft-checkbox-holder">
                    <input type="checkbox" id="ft_checkbox_0" className="ft-checkbox" />  
                    <label className="ft-checkbox__label" htmlFor="ft_checkbox_0">&nbsp;</label> 
                  </div>
                </Col>
                <Col className="ft-col_title" columnTitle={"title"}>Title</Col>
                <Col columnTitle={"last-updated-in"}>Last Updated In</Col>
                <Col columnTitle={"last-modified-by"}>Last Modified by</Col>
                <Col columnTitle={"last-modified"}>Last Modified</Col>
                <Col className="ft-col_no-flex-grow" columnTitle={"type"}>Type</Col>
                <Col className="ft-col_no-flex-grow" columnTitle={"size"}>Size</Col>
              </Row>

              {sampleData.map((data, i) => {
                return (
                  <Row key={(i+1)}>
                    <Col className="ft-col_checkbox" columnTitle="checkbox" mobileCompactListHeader>
                      <div className="ft-checkbox-holder">
                        <input type="checkbox" id={"ft_checkbox_" + (i+1)} className="ft-checkbox" />  
                        <label className="ft-checkbox__label" htmlFor={"ft_checkbox_" + (i+1)}>&nbsp;</label> 
                      </div>
                    </Col>
                    <Col className="ft-col_title" columnTitle={"title"} fileType={data.fileType} mobileCompactListHeader>
                      <a>{data.title}</a>
                    </Col>
                    <Col columnTitle={"last-updated-in"}>{data.lastUpdatedIn}</Col>
                    <Col columnTitle={"last-modified-by"}>{data.lastModifiedBy}</Col>
                    <Col columnTitle={"last-modified"}>{data.lastModified}</Col>
                    <Col className="ft-col_no-flex-grow" columnTitle={"type"}>{data.fileType}</Col>
                    <Col className="ft-col_no-flex-grow" columnTitle={"size"}>{data.fileSize}</Col>
                  </Row>
                );
              })}
            </Table>
          </div>

          <div className="box">
            <h3>Props and CSS Classes</h3>
            <h4>Table</h4>
            <Table mobileCompactList>
              <Row rowHeader>
                <Col className="ft-col_with-border" columnTitle="props">Props</Col>
                <Col className="ft-col_with-border" columnTitle="type">Type</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Description</Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>mobileCompactList</Col>
                <Col className="ft-col_with-border" columnTitle="type">Boolean</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">If true, mobile renders to compact list view. Best use case when all data needs to display on mobile.</Col>
              </Row>
            </Table>

            <h4>Row</h4>
            <Table mobileCompactList>
              <Row rowHeader>
                <Col className="ft-col_with-border" columnTitle="props">Props</Col>
                <Col className="ft-col_with-border" columnTitle="type">Type</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Description</Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>rowHeader</Col>
                <Col className="ft-col_with-border" columnTitle="type">Boolean</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">If true, row becomes table header. Should be placed in the first row of a table. If table has property <strong>mobileCompactList</strong>, this row gets hidden. Equivalent to <strong>thead</strong></Col>
              </Row>
            </Table>

            <h4>Col</h4>
            <h5>Props</h5>
            <Table mobileCompactList>
              <Row rowHeader>
                <Col className="ft-col_with-border" columnTitle="props">Props</Col>
                <Col className="ft-col_with-border" columnTitle="type">Type</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Description</Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>mobileCompactListHeader</Col>
                <Col className="ft-col_with-border" columnTitle="type">Boolen</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">If true and table has property <strong>mobileCompactList</strong>, mobile renders column as table header on compact list view. Ideally placed in the first two columns of the 2nd row of a table, that <strong>has no rowHeader</strong> property.</Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>columnTitle</Col>
                <Col className="ft-col_with-border" columnTitle="type">String</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Column label for mobile compact list view. <br/> <strong>Ex:</strong> <em>columnTitle="last-modified-by"</em></Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>fileType</Col>
                <Col className="ft-col_with-border" columnTitle="type">String</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Value determines which icon to display. iD CMS specific. <br/> <strong>Ex:</strong> <em>fileType="docx"</em></Col>
              </Row>
            </Table>
            <h5>Classes</h5>
            <Table mobileCompactList>
              <Row rowHeader>
                <Col className="ft-col_with-border" columnTitle="props">CSS classes</Col>
                <Col className="ft-col_with-border" columnTitle="type">Type</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Description</Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>ft-col_title</Col>
                <Col className="ft-col_with-border" columnTitle="type">iD CMS Specific</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Title column styling.</Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>ft-col_checkbox</Col>
                <Col className="ft-col_with-border" columnTitle="type">iD CMS Specific</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Checkbox column styling.</Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>ft-col_with-border</Col>
                <Col className="ft-col_with-border" columnTitle="type">Utility</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Adds border to a column.</Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>ft-col_no-flex-grow</Col>
                <Col className="ft-col_with-border" columnTitle="type">Utility</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Sets <strong>flex-grow:0</strong> and defaults column width to auto.</Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>ft-col_flex-grow-2</Col>
                <Col className="ft-col_with-border" columnTitle="type">Utility</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Sets <strong>flex-grow:2</strong> and column width grows to factor of 2.</Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>ft-show_xs</Col>
                <Col className="ft-col_with-border" columnTitle="type">Utility</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Displays chosen columns from   <strong>smallest breakpoint</strong> and above.</Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>ft-show_sm</Col>
                <Col className="ft-col_with-border" columnTitle="type">Utility</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Displays chosen columns from   <strong>480px</strong> and above.</Col>
              </Row>
              <Row>
                <Col className="ft-col_with-border" columnTitle="props" mobileCompactListHeader>ft-show_md</Col>
                <Col className="ft-col_with-border" columnTitle="type">Utility</Col>
                <Col className="ft-col_flex-grow-2 ft-col_with-border" columnTitle="description">Displays chosen columns from   <strong>768px</strong> and above.</Col>
              </Row>
            </Table>
          </div>

        </div>
      </div>
    );
  }
}
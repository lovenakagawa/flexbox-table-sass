import React, { Component } from 'react';

export class Col extends Component {
    renderClassNames = (props) => {
        let mobileCompactListHeader = this.props.mobileCompactListHeader ? 'ft-col_header ' : '';
        let addClassName = this.props.className ? this.props.className : '';
        return mobileCompactListHeader + addClassName;
    }

    assignDataLabel = (props) => {
        let dataTitle = this.props.columnTitle.split("-");
        return dataTitle.join(" ");
    }

    render() {
        return (
            <div 
                className={'ft-col ' + (this.renderClassNames())}
                data-column-title={this.props.columnTitle}
                data-column-label={this.assignDataLabel()}
                data-file-type={(this.props.fileType)}
            >
                {this.props.children}
            </div>
        );
    }
}

export class Row extends Component {
    renderClassNames = (props) => {
        let rowHeader = this.props.rowHeader ? 'ft-row_header ' : '';
        let addClassName = this.props.className ? this.props.className : '';
        return rowHeader + addClassName;
    }

    render() {
        return (
            <div className={'ft-row ' + (this.renderClassNames())}>
                {this.props.children}
            </div>
        );
    }
}

export class Table extends Component {
    renderClassNames = (props) => {
        let mobileCompactList = this.props.mobileCompactList ? 'flextable_compact-list ' : '';
        let addClassName = this.props.className ? this.props.className : '';
        return mobileCompactList + addClassName;
    }

    render() {
        return (
            <div className={'flextable ' + (this.renderClassNames())}>        
                {this.props.children}           
            </div>
        );
    }
}